# Thema
Flappy Owl (ein «Flappy Bird»-Ableger)

---

# Liste der Teammitglieder

- Justin Anderwert (anderjus)
- Nathanael Erdin (erdinnat)

---

# Projektergebnis: Link auf Repository

[GitLab-Repository](https://gitlab.com/lolzk/flappy-owl)

---

# Projektergebnis: Screenshots

### Start Screen
![Start Screen](img/screenshot_start.png "Start Screen")

### Game Screen
![Game Screen](img/screenshot_game.png "Game Screen")

### Game Over Screen
![Game Over Screen](img/Startseite_game_over.png "Game Over Screen")

---

# Projektergebnis: Erfahrungen

Wir haben das Spiel von Grund auf aufgesetzt und wissen nun, wie man ein Spiel from Scratch aufsetzen kann.

- Wir konnten diverse Erfahrungen sammmeln im Umgang mit
	- Android Studio
	- Adobe InDesign
	- Java
	- Frames per second
	- Darstellen von Bildern auf dem Screen
	- Android API (Shared Preferences um Daten auf dem Gerät zu speichern)
	- Design Patterns
	- Collision Detection

---

# Bemerkungen
	
Anfangs war es etwas frustrierend mit all den neuen Technologien, letztendlich hat es aber sehr viel Spass gemacht und Rückblickend war es eine tolle Erfahrung. Wir freuen uns auf `MOBA2`.
