package com.example.flappyowl;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.example.flappyowl.sprites.Background;
import com.example.flappyowl.sprites.GameOver;
import com.example.flappyowl.sprites.Obstacle;
import com.example.flappyowl.sprites.ObstacleManager;
import com.example.flappyowl.sprites.Owl;
import com.example.flappyowl.sprites.GameStart;
import com.example.flappyowl.sprites.Score;

import java.util.ArrayList;
import java.util.HashMap;

public class GameManager extends SurfaceView implements SurfaceHolder.Callback, PositionObserver {

    private static final String APP_NAME = "Flappy Owl";
    public MainThread thread;

    private GameState gameState = GameState.GAME_START;

    private Owl owl;
    private Background background;
    private ObstacleManager obstacleManager;
    private GameOver gameOver;
    private GameStart gameStart;
    private Score scoreSprite;

    private int score;
    private Rect owlPosition;
    private HashMap<Obstacle, ArrayList<Rect>> obstaclePositions = new HashMap<>();

    private DisplayMetrics dm;

    public GameManager(Context context, AttributeSet attributeSet) {
        super(context);
        getHolder().addCallback(this);
        thread = new MainThread(this, getHolder());
        dm = new DisplayMetrics();
        ((Activity)context).getWindowManager().getDefaultDisplay().getMetrics(dm);

        initGame();
    }

    private void initGame() {
        score = 0;
        owlPosition = new Rect();
        obstaclePositions = new HashMap<>();

        owl = new Owl(getResources(), dm.heightPixels, this);
        background = new Background(getResources(), dm.heightPixels);
        obstacleManager = new ObstacleManager(getResources(), dm.widthPixels, dm.heightPixels, this);
        gameOver = new GameOver(getResources(), dm.widthPixels, dm.heightPixels);
        gameStart = new GameStart(getResources(), dm.widthPixels, dm.heightPixels);
        scoreSprite = new Score(getResources(),dm.widthPixels, dm.heightPixels);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        if(thread.getState() == Thread.State.TERMINATED)
            thread = new MainThread(this, holder);

        thread.setRunning(true);
        thread.start();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        boolean retry = true;

        while(retry) {
            try {
                thread.setRunning(false);
                thread.join();
            }
            catch(InterruptedException ex) {
                ex.printStackTrace();
            }
            //wtf
            retry = false;
        }
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);

        background.draw(canvas);

        switch(gameState) {
            case GAME_START:
                gameStart.draw(canvas);
                break;
            case PLAYING:
                owl.draw(canvas);
                obstacleManager.draw(canvas);
                scoreSprite.draw(canvas);
                calculateCollision();
                break;
            case GAME_OVER:
                owl.draw(canvas);
                obstacleManager.draw(canvas);
                gameOver.draw(canvas);
                scoreSprite.draw(canvas);
                break;
        }
    }

    public void update() {
        switch(gameState) {
            case PLAYING:
                owl.update();
                obstacleManager.update();
                break;
            case GAME_OVER:
                owl.update();
                break;
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch(gameState){
            case GAME_START:
                initGame();
                gameState = GameState.PLAYING;
                break;
            case PLAYING:
                owl.onTouchEvent();
                break;
            case GAME_OVER:
                gameState = GameState.GAME_START;
                break;
        }

        return super.onTouchEvent(event);
    }

    @Override
    public void updateBirdPosition(Rect birdPosition) {
        this.owlPosition = birdPosition;
    }

    @Override
    public void updateObstaclePosition(Obstacle obstacle, ArrayList<Rect> currentObstaclePositions) {
        if(this.obstaclePositions.containsKey(obstacle))
            this.obstaclePositions.remove(obstacle);

        this.obstaclePositions.put(obstacle, currentObstaclePositions);
    }

    @Override
    public void removeObstacle(Obstacle obstacle) {
        obstaclePositions.remove(obstacle);
        score++;
        scoreSprite.setScore(score);
    }

    public void calculateCollision() {
        boolean collision = false;

        if(owlPosition.bottom > dm.heightPixels) {
            collision = true;
        }

        else {
            for(Obstacle obstacle : obstaclePositions.keySet()) {
                Rect bottomRectangle = obstaclePositions.get(obstacle).get(0);
                Rect topRectangle = obstaclePositions.get(obstacle).get(1);

                if(owlPosition.right > bottomRectangle.left
                        && owlPosition.left < bottomRectangle.right
                        && owlPosition.bottom > bottomRectangle.top){
                    collision = true;
                } else if (owlPosition.right > topRectangle.left
                        && owlPosition.left < topRectangle.right
                        && owlPosition.top < topRectangle.bottom) {
                    collision = true;
                }
            }
        }

        if(collision) {
            gameState = GameState.GAME_OVER;
            owl.collision();
            scoreSprite.collision(getContext().getSharedPreferences(APP_NAME, Context.MODE_PRIVATE));
        }
    }
}
