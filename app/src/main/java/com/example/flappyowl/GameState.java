package com.example.flappyowl;

public enum GameState {
    GAME_START,
    PLAYING,
    GAME_OVER,
}
