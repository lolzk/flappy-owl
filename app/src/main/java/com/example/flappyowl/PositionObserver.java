package com.example.flappyowl;

import android.graphics.Rect;

import com.example.flappyowl.sprites.Obstacle;

import java.util.ArrayList;

public interface PositionObserver {
    void updateBirdPosition(Rect birdPosition);
    void updateObstaclePosition(Obstacle obstacle, ArrayList<Rect> currentObstaclePositions);
    void removeObstacle(Obstacle obstacle);
}
