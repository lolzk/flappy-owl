package com.example.flappyowl.sprites;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;

import com.example.flappyowl.PositionObserver;
import com.example.flappyowl.R;

public class Owl implements Sprite {

    private Bitmap owlDown;
    private Bitmap owlUp;
    private PositionObserver positionObserver;

    private int pX;
    private int pY;
    private int width;
    private int height;

    private float g;
    private float vFall;
    private float owlPush;
    private boolean collision = false;
    private int screenHeight;

    public Owl(Resources resources, int screenHeight, PositionObserver positionObserver) {
        this.screenHeight = screenHeight;
        this.positionObserver = positionObserver;

        pX = (int) resources.getDimension(R.dimen.owl_x);
        width = (int) resources.getDimension(R.dimen.owl_width);
        height = (int) resources.getDimension(R.dimen.owl_height);

        g = (int) resources.getDimension(R.dimen.g);
        owlPush = (int) resources.getDimension(R.dimen.owl_push);

        Bitmap owlDownMap = BitmapFactory.decodeResource(resources, R.drawable.owl_down);
        owlDown = Bitmap.createScaledBitmap(owlDownMap, width, height, false);

        Bitmap owlUpMap = BitmapFactory.decodeResource(resources, R.drawable.owl_up);
        owlUp = Bitmap.createScaledBitmap(owlUpMap, width, height, false);
    }

    private boolean isOwlAtBottom() {
        return pY + owlDown.getHeight() >= screenHeight;
    }

    @Override
    public void draw(Canvas canvas) {
        if(vFall < 0)
            canvas.drawBitmap(owlUp, pX, pY, null);
        else
            canvas.drawBitmap(owlDown, pX, pY, null);
    }

    @Override
    public void update() {
        if(!isOwlAtBottom()) {
            pY += vFall;
            vFall += g;
        }

        if(!collision)
            positionObserver.updateBirdPosition(new Rect(pX, pY, pX + width, pY + height));
    }

    public void onTouchEvent() {
        vFall = owlPush;
    }

    public void collision() {
        collision = true;
    }
}
