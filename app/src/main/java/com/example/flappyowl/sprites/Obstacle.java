package com.example.flappyowl.sprites;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

import com.example.flappyowl.R;

import java.util.ArrayList;
import java.util.Random;

public class Obstacle implements Sprite {

    private Bitmap obstacle;
    private Bitmap obstacleTopHead;
    private Bitmap obstacleBottomHead;
    private ObstacleObserver obstacleObserver;

    private int width;
    private int heightBottomTree;
    private int headHeight;
    private int separation;
    private int yMin;
    private int pX;
    private int vX;

    private int screenWidth;
    private int screenHeight;


    public Obstacle(Resources resources, int screenWidth, int screenHeight, ObstacleObserver obstacleObserver) {
        this.screenHeight = screenHeight;
        this.screenWidth = screenWidth;
        this.obstacleObserver = obstacleObserver;

        pX = screenWidth;
        vX = (int) resources.getDimension(R.dimen.obstacle_v_x);
        width = (int) resources.getDimension(R.dimen.obstacle_width);
        separation = (int) resources.getDimension(R.dimen.obstacle_separation);
        headHeight = (int) resources.getDimension(R.dimen.head_height);
        yMin = (int) resources.getDimension(R.dimen.obstacle_y_min);

        obstacle = BitmapFactory.decodeResource(resources, R.drawable.obstacle);
        obstacleTopHead = BitmapFactory.decodeResource(resources, R.drawable.obstacle_top_head);
        obstacleBottomHead = BitmapFactory.decodeResource(resources, R.drawable.obstacle_bottom_head);
        generateRandomHeight();
    }

    private void generateRandomHeight() {
        Random random = new Random(System.currentTimeMillis());
        heightBottomTree = random.nextInt(screenHeight - 2* yMin - separation) + yMin;
    }

    @Override
    public void draw(Canvas canvas) {
        Rect bottomTree = new Rect(
                pX,
                screenHeight - heightBottomTree,
                pX + width,
                screenHeight);
        Rect bottomHead = new Rect(
                pX,
                screenHeight - heightBottomTree - headHeight,
                pX + width,
                screenHeight - heightBottomTree);

        Rect topTree = new Rect(
                pX,
                0,
                pX + width,
                screenHeight - heightBottomTree - separation - 2*headHeight);
        Rect topHead = new Rect(
                pX,
                screenHeight - heightBottomTree - separation - 2*headHeight,
                pX + width,
                screenHeight - heightBottomTree - separation - headHeight);

        Paint paint = new Paint();
        canvas.drawBitmap(obstacle, null, bottomTree, paint);
        canvas.drawBitmap(obstacleBottomHead, null, bottomHead, paint);
        canvas.drawBitmap(obstacle, null, topTree, paint);
        canvas.drawBitmap(obstacleTopHead, null, topHead, paint);
    }

    @Override
    public void update() {
        pX -= vX;

        if(pX <= 0 - width) {
            obstacleObserver.offScreen(this);
            return;
        }

        ArrayList<Rect> positions = new ArrayList<>();

        Rect bottomPosition = new Rect(
                pX,
                screenHeight - heightBottomTree - headHeight / 2,
                pX + width,
                screenHeight);
        Rect topPosition = new Rect(
                pX,
                0,
                pX + width,
                screenHeight - heightBottomTree - headHeight / 2 - separation);

        positions.add(bottomPosition);
        positions.add(topPosition);

        obstacleObserver.updatePosition(this, positions);
    }
}
