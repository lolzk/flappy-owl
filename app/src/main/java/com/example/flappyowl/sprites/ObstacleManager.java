package com.example.flappyowl.sprites;

import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Rect;

import com.example.flappyowl.PositionObserver;
import com.example.flappyowl.R;

import java.util.ArrayList;

public class ObstacleManager implements ObstacleObserver {

    private int interval;
    private int progress;
    private int vX;
    private int screenWidth;
    private int screenHeight;
    private Resources resources;
    private PositionObserver positionObserver;
    private ArrayList<Obstacle> obstacles = new ArrayList<>();

    public ObstacleManager(Resources resources, int screenWidth, int screenHeight, PositionObserver positionObserver) {
        this.resources = resources;
        this.screenWidth = screenWidth;
        this.screenHeight = screenHeight;
        this.positionObserver = positionObserver;

        progress = 0;
        vX = (int) resources.getDimension(R.dimen.obstacle_v_x);
        interval = (int) resources.getDimension(R.dimen.obstacle_interval);

        obstacles.add(new Obstacle(resources, screenWidth, screenHeight, this));
    }

    public void draw(Canvas canvas) {
        for(Obstacle obstacle : obstacles) {
            obstacle.draw(canvas);
        }
    }

    public void update() {
        progress += vX;

        if(progress > interval) {
            progress = 0;
            obstacles.add(new Obstacle(resources, screenWidth, screenHeight, this));
        }

        // prevent concurrency problems, because list could get changed in an other thread
        ArrayList<Obstacle> temp = new ArrayList<>();
        temp.addAll(obstacles);

        for(Obstacle obstacle : temp) {
            obstacle.update();
        }
    }

    @Override
    public void offScreen(Obstacle obstacle) {
        obstacles.remove(obstacle);
        positionObserver.removeObstacle(obstacle);
    }

    @Override
    public void updatePosition(Obstacle obstacle, ArrayList<Rect> positions) {
        positionObserver.updateObstaclePosition(obstacle, positions);
    }
}
