package com.example.flappyowl.sprites;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;

import com.example.flappyowl.R;

public class Background implements Sprite {

    private Bitmap sky;
    private Bitmap ground;
    private int skyHeight;
    private int groundHeight;

    private int screenHeight;

    public Background(Resources resources,int screenHeight) {
        this.screenHeight = screenHeight;

        skyHeight = screenHeight;
        groundHeight = (int) resources.getDimension(R.dimen.bg_bottom_height);

        Bitmap skyBitmap = BitmapFactory.decodeResource(resources, R.drawable.sky);
        Bitmap groundBitmap = BitmapFactory.decodeResource(resources, R.drawable.ground);

        sky = Bitmap.createScaledBitmap(skyBitmap, skyBitmap.getWidth(), skyHeight, false);
        ground = Bitmap.createScaledBitmap(groundBitmap, groundBitmap.getWidth(), skyHeight, false);
    }

    @Override
    public void draw(Canvas canvas) {
        canvas.drawBitmap(sky, 0, 0, null);
        // for large tablets if the bitmap is to small
        canvas.drawBitmap(sky, sky.getWidth(), 0, null);

        canvas.drawBitmap(ground, 0, screenHeight - ground.getHeight(), null);
        canvas.drawBitmap(ground, ground.getWidth(), screenHeight - ground.getHeight(), null);
    }

    @Override
    public void update() {

    }
}
