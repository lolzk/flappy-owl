package com.example.flappyowl.sprites;

import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;

import com.example.flappyowl.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class Score implements Sprite {

    private static final String SCORE_PREF = "FlappyOwl Score";

    private Resources resources;
    private int screenHeight;
    private int screenWidth;

    private int score;
    private int topScore;
    private boolean collision = false;

    private HashMap<Integer, Bitmap> digitsMap = new HashMap<>();

    public Score(Resources resources, int screenWidth, int screenHeight) {
        this.resources = resources;
        this.screenHeight = screenHeight;
        this.screenWidth = screenWidth;

        digitsMap.put(0, BitmapFactory.decodeResource(resources, R.drawable.zahl_0));
        digitsMap.put(1, BitmapFactory.decodeResource(resources, R.drawable.zahl_1));
        digitsMap.put(2, BitmapFactory.decodeResource(resources, R.drawable.zahl_2));
        digitsMap.put(3, BitmapFactory.decodeResource(resources, R.drawable.zahl_3));
        digitsMap.put(4, BitmapFactory.decodeResource(resources, R.drawable.zahl_4));
        digitsMap.put(5, BitmapFactory.decodeResource(resources, R.drawable.zahl_5));
        digitsMap.put(6, BitmapFactory.decodeResource(resources, R.drawable.zahl_6));
        digitsMap.put(7, BitmapFactory.decodeResource(resources, R.drawable.zahl_7));
        digitsMap.put(8, BitmapFactory.decodeResource(resources, R.drawable.zahl_8));
        digitsMap.put(9, BitmapFactory.decodeResource(resources, R.drawable.zahl_9));
    }

    @Override
    public void draw(Canvas canvas) {
        ArrayList<Bitmap> digits = getScoreBitmaps(score);
        int digitWidth = digitsMap.get(0).getWidth();

        if(!collision) {
            for(int i = 0; i < digits.size(); i++) {
                int x = screenWidth / 2 - digits.size() * digitWidth / 2 + (digitWidth + 15) * i;
                canvas.drawBitmap(digits.get(i), x, screenHeight / 6, null);
            }
        }
        else {
            ArrayList<Bitmap> topScoreDigits = getScoreBitmaps(topScore);
            int y = screenHeight * 3 / 4;

            // score
            for(int i = 0; i < digits.size(); i++) {
                int x = screenWidth / 6 - digits.size() * digitWidth / 2 + (digitWidth + 15) * i;
                canvas.drawBitmap(digits.get(i), x, y, null);
            }

            // topScore
            for(int i = 0; i < topScoreDigits.size(); i++) {
                int x = 4 * screenWidth / 6 - digits.size() * digitWidth / 2 + (digitWidth + 15) * i;
                canvas.drawBitmap(topScoreDigits.get(i), x, y, null);
            }

            return;
        }
    }

    private ArrayList<Bitmap> getScoreBitmaps(int score) {
        ArrayList<Bitmap> digits = new ArrayList<>();
        int runner = score;

        if(runner == 0) {
            digits.add(digitsMap.get(0));
        }

        while(runner > 0) {
            int digit = runner % 10;
            runner /= 10;
            digits.add(digitsMap.get(digit));
        }

        Collections.reverse(digits);
        return digits;
    }

    @Override
    public void update() {
    }

    public void setScore(int score) {
        this.score = score;
    }

    public void collision(SharedPreferences preferences) {
        collision = true;
        topScore = preferences.getInt(SCORE_PREF, 0);
        if(topScore < score) {
            preferences.edit().putInt(SCORE_PREF, score).apply();
            topScore = score;
        }
    }
}
