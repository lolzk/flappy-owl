package com.example.flappyowl.sprites;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;

import com.example.flappyowl.R;

public class GameOver implements Sprite {

    private Bitmap image;

    private Resources resources;
    private int screenWidth;
    private int screenHeight;
    private int width;
    private int height;

    public GameOver(Resources resources, int screenWidth, int screenHeight) {
        this.resources = resources;
        this.screenWidth = screenWidth;
        this.screenHeight = screenHeight;

        width = (int) resources.getDimension(R.dimen.banner_width);
        height = (int) resources.getDimension(R.dimen.banner_height);

        image = BitmapFactory.decodeResource(resources, R.drawable.game_over);
        image = Bitmap.createScaledBitmap(image, width, height, false);
    }

    @Override
    public void draw(Canvas canvas) {
        canvas.drawBitmap(image, screenWidth/2 - image.getWidth()/2, screenHeight/5, null);
    }

    @Override
    public void update() {

    }
}
