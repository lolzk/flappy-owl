package com.example.flappyowl.sprites;

import android.graphics.Canvas;

public interface Sprite {
    void draw(Canvas canvas);
    void update();
}
