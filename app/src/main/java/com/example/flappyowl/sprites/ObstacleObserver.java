package com.example.flappyowl.sprites;

import android.graphics.Rect;

import java.util.ArrayList;

public interface ObstacleObserver {
    void offScreen(Obstacle obstacle);
    void updatePosition(Obstacle obstacle, ArrayList<Rect> positions);
}
