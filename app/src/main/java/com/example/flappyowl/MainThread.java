package com.example.flappyowl;

import android.graphics.Canvas;
import android.view.SurfaceHolder;

public class MainThread extends Thread {

    private static Canvas canvas = null;
    private static int fps = 60;

    private GameManager gameManager;
    private SurfaceHolder surfaceHolder;
    private boolean running;

    public MainThread(GameManager gameManager, SurfaceHolder surfaceHolder) {
        this.gameManager = gameManager;
        this.surfaceHolder = surfaceHolder;
    }

    public void setRunning(boolean isRunning) {
        running = isRunning;
    }

    @Override
    public void run() {
        long startTime;
        long targetTimeInMs = 1000 / fps;
        long waitTimeInMs;
        long timeInMs;

        while(running) {
            startTime = System.nanoTime();
            canvas = null;

            try {
                canvas = surfaceHolder.lockCanvas();

                synchronized (surfaceHolder) {
                    gameManager.update();
                    gameManager.draw(canvas);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                if (canvas != null) {
                    try {
                        surfaceHolder.unlockCanvasAndPost(canvas);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            timeInMs = (System.nanoTime() - startTime) / 1000000;
            waitTimeInMs = targetTimeInMs - timeInMs;

            try {
                // approximate fps
                if(waitTimeInMs > 0)
                    sleep(waitTimeInMs);
            }
            catch(Exception ex) {
                ex.printStackTrace();
            }
        }
    }
}
